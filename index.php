<?php get_header(); ?>

    <main id="content" role="main">
        <h1>DIGITAL SIGNAGE SCREEN</h1>

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php endwhile; endif; ?>

        <?php
        echo '<ul>';
        foreach (get_categories() as $category) {
            echo '<li>';
            echo '<a href="' . get_category_link($category) . '" class="category-link">' .
                esc_html($category->name) .
                '</a>';
            echo '</li>';
        }
        echo '</ul>';
        ?>

    </main>

<?php get_footer(); ?>