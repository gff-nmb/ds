# DS Theme

The WordPress Theme **DS**.

## Dependencies

* [DS Core](https://gitlab.com/gff-nmb/ds-core)

## Authors and acknowledgment

Adrian Suter, [adrian.suter@gff.ch](mailto:adrian.suter@gff.ch)

## Copyright

[GFF Integrative Kommunikation GmbH](https://www.gff.ch/)

## License

[GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

## Development

### Staging

Always use a staging environment to develop and test things out before releasing.

### Build a new release

1. Set the new version `X.Y` in the file comments section of `_dev/scss/style.scss`.
   Commit and push.
2. Add a new repository tag `X.Y` without writing release notes (a message is
   allowed).
3. Gitlab CI would automatically create a release asset zip file.
4. [Optional] Once finished (see pipeline or release section), update the stable version in
   `readme.txt` to `X.Y` and commit and push.

### Notes

#### Device screen sizes

The size of the fully browser for the Samsung Galaxy Tab A7 (2021) tablet is 1334 &times; 800 pixels.

The size for R.1.0.0 (Infostele) corresponds to 725 &times; 1280 pixels.

[http://howbigismybrowser.com/](http://howbigismybrowser.com/)

#### Video converter

```bash
ffmpeg -i input.mov -c:v libvpx -crf 10 -b:v 5M -c:a libvorbis output.webm
```
