<?php

declare(strict_types=1);

namespace DS;

use RuntimeException;

class Block_Pattern_Manager
{
    public static function register(string $name, string $title, ?string $description = null): void
    {
        $file = DS_THEME_DIR_PATH . 'block-patterns/' . str_replace('_', '-', $name) . '.php';
        if (!file_exists($file)) {
            throw new RuntimeException('Block pattern can not be registered.');
        }

        ob_start();
        include($file);
        $content = ob_get_clean();

        $properties = [
            'title' => $title,
            'content' => $content,
            'categories' => ['ds'],
        ];

        if (!is_null($description)) {
            $properties['description'] = $description;
        }

        register_block_pattern($name, $properties);
    }
}
