<?php

declare( strict_types=1 );

namespace DS;

class SVG_Utils {
	public static function ds_get_svg( string $name, bool $remove_style_nodes = true ): string {
		$file_path = DS_THEME_DIR_PATH . 'assets/svg/' . $name . '.svg';
		if ( ! is_readable( $file_path ) ) {
			return '';
		}

		$code = file_get_contents( $file_path );
		if ( $remove_style_nodes ) {
			return preg_replace( '/<style>(.*)<\/style>/isu', '', $code );
		}

		return $code;
	}

	public static function ds_svg( string $name, bool $remove_style_nodes = true ): void {
		echo self::ds_get_svg( $name );
	}
}
