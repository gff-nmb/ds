<?php

declare( strict_types=1 );

namespace DS\shortcodes;

class Museum_Map {

	protected const SCRIPT_HANDLE = 'ds-museum-map';

	protected const STYLE_HANDLE = 'ds-museum-map';

	protected const OPTION_SCRIPT_HANDLE = 'ds-museum-map-option';

	protected const OPTION_STYLE_HANDLE = 'ds-museum-map-option';

	protected const OPTION_GROUP = 'ds_museum_map_group';

	protected const OPTION_NAME = 'ds_museum_map';

	protected const SECTION_LAYERS = 'ds_museum_map_layers';

	protected const FIELD_LAYERS = 'ds_museum_map_layers';

	public static function init(): void {
		add_shortcode( 'museum-map', [ self::class, 'render_museum_map' ] );
		add_action( 'admin_menu', [ self::class, 'admin_menu' ] );
		add_action( 'admin_init', [ self::class, 'admin_init' ] );

		wp_register_script(
			self::SCRIPT_HANDLE,
			get_theme_file_uri( 'assets/museum-map/museum-map.js' ),
			[ 'jquery', 'ds-script' ],
			1
		);
		wp_register_style( self::STYLE_HANDLE,
			get_theme_file_uri( 'assets/museum-map/museum-map.css' ),
			[ 'ds-style' ],
			1 );
	}

	public static function admin_menu(): void {
		add_options_page(
			__( 'Museum Map', 'ds' ),
			__( 'Museum Map', 'ds' ),
			'manage_options',
			'ds-museum-map',
			[ self::class, 'options_page_callback' ],
			999
		);
	}

	public static function admin_init(): void {
		register_setting(
			self::OPTION_GROUP,
			self::OPTION_NAME,
			[
				'type'              => 'object',
				'sanitize_callback' => [ self::class, 'sanitize_callback' ],
			]
		);

		add_settings_section(
			self::SECTION_LAYERS,
			__( 'Layers', 'ds' ),
			[ self::class, 'settings_section_callback' ],
			self::OPTION_NAME
		);

		add_settings_field(
			self::FIELD_LAYERS,
			__( 'Museum Map Layers', 'ds' ),
			[ self::class, 'settings_field_layers_callback' ],
			self::OPTION_NAME,
			self::SECTION_LAYERS,
		);

		wp_register_script(
			self::OPTION_SCRIPT_HANDLE,
			get_theme_file_uri( 'assets/museum-map/museum-map-option.js' ),
			[ 'jquery' ],
			true
		);

		wp_register_style(
			self::OPTION_STYLE_HANDLE,
			get_theme_file_uri( 'assets/museum-map/museum-map-option.css' ),
			[],
			true
		);
	}

	public static function options_page_callback(): void {
		wp_enqueue_script( self::OPTION_SCRIPT_HANDLE );
		wp_enqueue_style( self::OPTION_STYLE_HANDLE );
		?>
        <div class="wrap">
            <h1><?php
				_e( 'Settings' ); ?> › <?php
				_e( 'Museum Map', 'ds' ); ?></h1>

            <form method="post" action="<?php
			echo esc_url( admin_url( 'options.php' ) ); ?>" novalidate="novalidate">
				<?php
				settings_fields( self::OPTION_GROUP );
				do_settings_sections( self::OPTION_NAME );
				submit_button();
				?>
            </form>
        </div>
		<?php
	}

	public static function settings_section_callback(): void {
		?>
        <p>
			<?php
			_e( 'In this section you can define the museum map layers.', 'ds' ); ?>
        </p>
		<?php
	}

	public static function settings_field_layers_callback(): void {
		$options = get_option( self::OPTION_NAME );
		if ( ! is_countable( $options[ self::FIELD_LAYERS ] ) ) {
			$options[ self::FIELD_LAYERS ] = [];
		}
		?>
        <div data-ds-item>
            <div>
                <button data-ds-action="add" class="button button-secondary">+</button>
            </div>
        </div>
		<?php
		for ( $i = 0; $i < count( $options[ self::FIELD_LAYERS ] ); $i ++ ) {
			$item = $options[ self::FIELD_LAYERS ][ $i ];
			?>
            <div data-ds-item><?php
				self::render_field_layers_item( $item ); ?></div>
			<?php
		}

		?>
        <script id="ds-museum-map-layers-template" type="text/template">
			<?php
			self::render_field_layers_item( [] ); ?>
        </script>
		<?php
	}

	protected static function render_field_layers_item( array $item ): void {
		?>
        <div>
            <div>
                <input aria-label="" type="text" value="<?php
				echo esc_attr( $item['de'] ?? '' ); ?>" name="<?php
				echo self::OPTION_NAME; ?>[de][]" placeholder="Deutsche Bezeichnung">
            </div>
            <div>
                <input aria-label="" type="text" value="<?php
				echo esc_attr( $item['fr'] ?? '' ); ?>" name="<?php
				echo self::OPTION_NAME; ?>[fr][]" placeholder="Französische Bezeichnung">
            </div>
            <div>
                <input aria-label="" type="color" value="<?php
				echo esc_attr( $item['color'] ?? '' ); ?>" name="<?php
				echo self::OPTION_NAME; ?>[color][]" placeholder="Farbe">
            </div>
            <div>
                <input aria-label="" type="text" value="<?php
				echo esc_attr( $item['overlays'] ?? '' ); ?>" name="<?php
				echo self::OPTION_NAME; ?>[overlays][]" placeholder="Overlays">
            </div>
        </div>
        <div>
            <button data-ds-action="add" class="button button-secondary">+</button>
            <button data-ds-action="del" class="button button-secondary">-</button>
        </div>
		<?php
	}

	public static function sanitize_callback( $input ): array {
		$output = [
			self::FIELD_LAYERS => [],
		];

		if ( is_countable( $input['de'] ) ) {
			for ( $i = 0; $i < count( $input['de'] ); $i ++ ) {
				$output[ self::FIELD_LAYERS ][] = [
					'de'       => sanitize_text_field( $input['de'][ $i ] ),
					'fr'       => sanitize_text_field( $input['fr'][ $i ] ),
					'color'    => sanitize_hex_color( $input['color'][ $i ] ),
					'overlays' => sanitize_text_field( $input['overlays'][ $i ] ),
				];
			}
		}

		return $output;
	}

	protected static function remove_non_alpha_numeric_extended( string $input ): string {
		return preg_replace( '/[^A-Za-z0-9_\-]/', '_', $input );
	}

	protected static function render_svg( string $file_path, array $classes = [], array $attributes = [] ): string {
		$theme_file_path = get_theme_file_path( $file_path );
		if ( is_readable( $theme_file_path ) ) {
			$html = '<img src="' . get_theme_file_uri( $file_path ) .
			        '?v=' . filemtime( $theme_file_path ) . '" alt=""';
			if ( ! empty( $classes ) ) {
				$html .= ' class="' . implode( ' ', $classes ) . '"';
			}
			foreach ( $attributes as $key => $value ) {
				$html .= ' ' . $key . '="' . $value . '"';
			}
			$html .= '/>';

			return $html;
		}

		return '';
	}

	public static function render_museum_map( $atts ): string {
		$location = self::remove_non_alpha_numeric_extended(
			array_key_exists( 'location', $atts ) ? $atts['location'] : ''
		);

		$current_language           = wpml_get_current_language();
		$current_language_uppercase = self::remove_non_alpha_numeric_extended(
			strtoupper( $current_language )
		);

		wp_enqueue_script( self::SCRIPT_HANDLE );
		wp_enqueue_style( self::STYLE_HANDLE );

		$options = get_option( self::OPTION_NAME );
		if ( ! is_countable( $options[ self::FIELD_LAYERS ] ) ) {
			$options[ self::FIELD_LAYERS ] = [];
		}

		$config = $options[ self::FIELD_LAYERS ];

		ob_start();

		$overlay_groups = [];

		?>
        <div class="ds-museum-map-container">
            <div class="ds-museum-map">
                <div class="ds-museum-map-inner">
					<?php
					echo self::render_svg( 'assets/museum-map/Museumsplan_BG.svg' );
					echo self::render_svg( 'assets/museum-map/Museumsplan_' . $current_language_uppercase . '.svg',
						[ 'ds-museum-map-overlay', 'visible' ]
					);

					foreach ( $config as $overlay_definition ) {
						$overlay_group_index = count( $overlay_groups );
						$overlay_groups[]    = [
							'index' => $overlay_group_index,
							'label' => $overlay_definition[ $current_language ],
							'color' => $overlay_definition['color'],
						];

						$overlays = explode( ',', $overlay_definition['overlays'] );

						foreach ( $overlays as $overlay ) {
							echo self::render_svg( 'assets/museum-map/Museumsplan_' . $overlay . '.svg',
								[ 'ds-museum-map-overlay' ], [ 'data-ds-museum-map-group' => $overlay_group_index ]
							);
						}
					}

					echo self::render_svg( 'assets/museum-map/Museumsplan_' . $location . '_Standort_' . $current_language_uppercase . '.svg',
						[ 'ds-museum-map-overlay', 'visible' ]
					);
					?>
                </div>
            </div>
            <div class="ds-museum-map-buttons">
				<?php
				usort( $overlay_groups, function ( $a, $b ) {
					$t = strnatcmp( $a['label'], $b['label'] );
					if ( $t > 0 ) {
						return 1;
					}
					if ( $t < 0 ) {
						return - 1;
					}

					return 0;
				} );

				foreach ( $overlay_groups as $overlay_group ) {
					echo '<a class="ds-museum-map-button" href="#" data-ds-museum-map-toggle="' . $overlay_group['index'] . '" data-ds-museum-map-color="' . $overlay_group['color'] . '">' . $overlay_group['label'] . '</a>';
				}
				?>
            </div>
        </div>
		<?php
		return ob_get_clean();
	}
}
