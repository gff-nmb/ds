<?php

declare( strict_types=1 );

namespace DS;

class Detector {
	public const DEVICE_GALAXY_TAB_A7_10_40 = 'galaxy-tab-a7-10-40';
	public const DEVICE_XORO_MEGAPAD_3204 = 'xoro-megapad-3204';
	public const DEVICE_XORO_MEGAPAD_2404 = 'xoro-megapad-2404';
	public const DEVICE_XORO_MEGAPAD_1405 = 'xoro-megapad-1405';
	public const DEVICE_PM43F = 'pm43f';

	/**
	 * Detects the device (used mainly to set appropriate styles) from the current
	 * category.
	 *
	 * @return string|null
	 */
	public static function detect_device(): ?string {
		$category_description = category_description();

		if (
			stripos( $category_description, 'galaxy-tab-a7-10-40' ) !== false ||
			stripos( $category_description, 'galaxy-tab-a8-10-50' ) !== false
		) {
			return self::DEVICE_GALAXY_TAB_A7_10_40;
		} elseif ( stripos( $category_description, 'xoro-megapad-3204' ) !== false ) {
			return self::DEVICE_XORO_MEGAPAD_3204;
		} elseif ( stripos( $category_description, 'xoro-megapad-2404' ) !== false ) {
			return self::DEVICE_XORO_MEGAPAD_2404;
		} elseif (
			stripos( $category_description, 'xoro-megapad-1405' ) !== false ||
			stripos( $category_description, 'xoro-megapad-1404' ) !== false
		) {
			return self::DEVICE_XORO_MEGAPAD_1405;
		} elseif ( stripos( $category_description, 'pm43f' ) !== false ) {
			return self::DEVICE_PM43F;
		}

		return null;
	}

	/**
	 * @param string      $value The value. Only a-z, A-Z, 0-9, _ and - are allowed.
	 * @param string|null $category_description
	 *
	 * @return bool
	 */
	protected static function detect_value( string $value, ?string $category_description = null ): bool {
		if ( is_null( $category_description ) ) {
			$category_description = category_description();
		}

		// Sanitize the given value.
		$value = preg_replace( '/[^a-zA-Z0-9_-]/s', '', $value );

		return boolval( preg_match( '/(^|\s|,|>)' . $value . '($|\s|,|<)/', $category_description ) );
	}

	/**
	 * Detects the world.
	 *
	 * @param string|null $category_description
	 *
	 * @return string|null
	 */
	public static function detect_world( ?string $category_description = null ): ?string {
		$worlds = [
			'world-1'  => 'world-1', // Wasser
			'world-2'  => 'world-2', // Röstigraben
			'world-3'  => 'world-3',
			'world-4'  => 'world-4',
			'world-5'  => 'world-5',
			'world-6'  => 'world-6',
			'world-7'  => 'world-7',
			'world-98' => 'world-98', // RUB.Fleisch
			'world-99' => 'world-99', // RUB.1
		];

		foreach ( $worlds as $world_name => $world_description_tag ) {
			if ( self::detect_value( $world_description_tag, $category_description ) ) {
				return $world_name;
			}
		}

		return null;
	}

	/**
	 * Detects whether the screen is marked as "autoplay". If yes, then the first found media file on the home "post"
	 * would be directly opened and automatically played.
	 *
	 * @param string|null $category_description
	 *
	 * @return bool
	 */
	public static function detect_autoplay( ?string $category_description = null ): bool {
		return self::detect_value( 'autoplay', $category_description );
	}

	/**
	 * Detects whether the screen is markes as "autoplay-gallery". If yes, then the first found media gallery on the
	 * home "post" would open directly in the fancybox fullscreen mode.
	 *
	 * @param string|null $category_description
	 *
	 * @return bool
	 */
	public static function detect_autoplay_gallery( ?string $category_description = null ): bool {
		return self::detect_value( 'autoplay-gallery', $category_description );
	}
}
