<?php

///
// NOTES:
// If a string needs to be replaced in the post content, then execute the following sql query.
//   UPDATE posts SET post_content = REPLACE(post_content, '<td>Fundort</td>', '<td>[label-location]</td>') WHERE post_type = "post";
///

declare( strict_types=1 );

use DS\Block_Pattern_Manager;
use DS\Detector;
use DS\shortcodes\Museum_Map;
use DS\SVG_Utils;
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;

define( 'DS_THEME_DIR_PATH', plugin_dir_path( __FILE__ ) );

include_once( DS_THEME_DIR_PATH . 'autoload.php' );

///////////////////////////// PLUGIN UPDATE CHECKER ////////////////////////////

add_action( 'after_setup_theme', function (): void {
	$group_name = 'gff-nmb';
	$theme_name = 'ds';
	if ( class_exists( PucFactory::class ) ) {
		$update_checker = PucFactory::buildUpdateChecker(
			'https://gitlab.com/' . $group_name . '/' . $theme_name . '/',
			__FILE__
		);

		$vcs_api = $update_checker->getVcsApi();
		if ( method_exists( $vcs_api, 'enableReleasePackages' ) ) {
			$vcs_api->enableReleasePackages();
		} else {
			trigger_error(
				'PUC Updater can not update as the VCS API does not support release packages.',
				E_USER_ERROR
			);
		}
	}
} );

///////////////////////////////////// MAIN /////////////////////////////////////

Museum_Map::init();

add_shortcode( 'label-artist', function (): string {
	return wpml_get_current_language() === 'de' ? 'Hersteller/-in / Künstler/-in' : 'Producteur/productrice / artiste';
} );

add_shortcode( 'label-object', function (): string {
	return wpml_get_current_language() === 'de' ? 'Gegenstand' : 'Objet';
} );

add_shortcode( 'label-location', function (): string {
	return wpml_get_current_language() === 'de' ? 'Fundort' : 'Lieu de découverte';
} );

add_shortcode( 'label-title', function (): string {
	return wpml_get_current_language() === 'de' ? 'Titel' : 'Titre';
} );

add_shortcode( 'label-date', function (): string {
	return wpml_get_current_language() === 'de' ? 'Datierung' : 'Datation';
} );

add_shortcode( 'label-material', function (): string {
	return wpml_get_current_language() === 'de' ? 'Material' : 'Matière';
} );

add_shortcode( 'label-technique', function (): string {
	return wpml_get_current_language() === 'de' ? 'Technik' : 'Technique';
} );

add_shortcode( 'label-owner', function (): string {
	return wpml_get_current_language() === 'de' ? 'Eigentümer/-in' : 'Propriétaire';
} );

add_shortcode( 'label-description', function (): string {
	return wpml_get_current_language() === 'de' ? 'Beschreibung' : 'Description';
} );

add_filter( 'body_class', function ( $classes ): array {
	if ( get_theme_mod( 'ds_development_mode', false ) ) {
		$classes[] = 'debug';
	}

	if ( ! is_category() ) {
		return $classes;
	}

	$device = Detector::detect_device();
	if ( ! is_null( $device ) ) {
		$classes[] = $device;
	}

	$world = Detector::detect_world();
	if ( ! is_null( $world ) ) {
		$classes[] = $world;
	}

	if ( Detector::detect_autoplay() ) {
		$classes[] = 'autoplay-screen';
	}

	if ( Detector::detect_autoplay_gallery() ) {
		$classes[] = 'autoplay-gallery-screen';
	}

	return $classes;
}, 10 );

add_action( 'wp_enqueue_scripts', function (): void {
	$version = wp_get_theme()->get( 'Version' );

	wp_enqueue_style( 'ds-style', get_stylesheet_uri(), [ 'wp-mediaelement' ], $version );

	if ( is_category() ) {
		$device = Detector::detect_device();
		if ( ! is_null( $device ) ) {
			wp_enqueue_style(
				'ds-style-device',
				get_theme_file_uri( 'devices/' . $device . '.css' ),
				[ 'ds-style' ],
				$version
			);
		}

		if ( ! is_null( filter_input( INPUT_GET, 'test' ) ) ) {
			wp_enqueue_script( 'ds-script-test', get_theme_file_uri( 'test/screen.js' ), [ 'ds-script' ], true );
		}
	}

	if ( is_front_page() ) {
		wp_enqueue_script( 'ds-script-test-runner', get_theme_file_uri( 'test/runner.js' ), [ 'jquery' ], true );
	}

	wp_enqueue_script( 'ds-script', get_theme_file_uri( 'script.js' ), [ 'wp-mediaelement' ], true, true );
	wp_localize_script( 'ds-script', 'ds', [
		'video' => [
			'close_button'   => '<div class="icon-button" data-action="close-video">' . SVG_Utils::ds_get_svg( 'button-arrow-left' ) . '</div>',
			'play_button'    => '<div class="icon-button">' . SVG_Utils::ds_get_svg( 'button-arrow-right' ) . '</div>',
			'pause_button'   => '<div class="icon-button">' . SVG_Utils::ds_get_svg( 'button-pause' ) . '</div>',
			'poster_default' => get_theme_file_uri( 'assets/svg/video-poster-default.svg' ),
		]
	] );

	if ( is_category() ) {
		$device = Detector::detect_device();
		if ( $device === Detector::DEVICE_PM43F ) {
			wp_enqueue_script(
				'ds-touch-animation',
				get_theme_file_uri( 'touch-animation.js' ),
				[ 'jquery' ],
				true,
				true
			);

			wp_enqueue_style( 'ds-touch-animation', get_theme_file_uri( 'touch-animation.css' ), [], true );
		}
	}
} );

add_action( 'after_setup_theme', function (): void {
	load_theme_textdomain( 'ds', get_template_directory() . '/languages' );

	add_theme_support( 'disable-custom-font-sizes' );
	add_theme_support( 'editor-font-sizes', [] );
	add_theme_support( 'disable-custom-colors' );
	add_theme_support( 'editor-color-palette', [
		[
			'name'  => __( 'Themenwelt: Wasser', 'ds' ),
			'slug'  => 'primary-w1',
			'color' => '#009881',
		],
		[
			'name'  => __( 'Themenwelt: Röstigraben', 'ds' ),
			'slug'  => 'primary-w2',
			'color' => '#bb792f',
		],
		[
			'name'  => __( 'Themenwelt: Rund um Biel (Fleisch)', 'ds' ),
			'slug'  => 'primary-w98',
			'color' => '#ff0054',
		],
		[
			'name'  => __( 'Themenwelt: Rund um Biel', 'ds' ),
			'slug'  => 'primary-w99',
			'color' => '#F29FC5',
		],
		[
			'name'  => __( 'Background Color', 'ds' ),
			'slug'  => 'background',
			'color' => '#000000',
		],
		[
			'name'  => __( 'Foreground Color', 'ds' ),
			'slug'  => 'foreground',
			'color' => '#ffffff',
		],
	] );

	add_theme_support( 'wp-block-styles' );
	add_theme_support( 'editor-styles' );
	add_editor_style( 'editor-style.css' );

	register_block_pattern_category( 'ds', [
		'label' => __( 'Digital Signage', 'ds' ),
	] );

	Block_Pattern_Manager::register(
		'front',
		__( 'Startseite', 'ds' )
	);

	Block_Pattern_Manager::register(
		'introduction',
		__( 'Einleitung', 'ds' )
	);

	Block_Pattern_Manager::register(
		'image_details',
		__( 'Legende (Kunst)', 'ds' )
	);

	Block_Pattern_Manager::register(
		'image_details_history',
		__( 'Legende (Archäologie/Geschichte)', 'ds' )
	);

	Block_Pattern_Manager::register(
		'video_multiple',
		__( 'Video (Startseite)', 'ds' )
	);

	Block_Pattern_Manager::register(
		'fav_location',
		__( 'Lieblingsort', 'ds' )
	);
} );

add_action( 'customize_register', function ( WP_Customize_Manager $wp_customize ): void {
	$section_development = $wp_customize->add_section( 'development', [
		'title'    => __( 'Development', 'ds' ),
		'priority' => 10,
	] );

	$wp_customize->add_setting( 'ds_development_mode', [
		'type'       => 'theme_mod',
		'capability' => 'edit_theme_options',
		'default'    => false,
		'transport'  => 'refresh',
	] );

	$wp_customize->add_control( 'ds_development_mode', [
		'type'        => 'checkbox',
		'priority'    => 10,
		'section'     => $section_development->id,
		'label'       => __( 'Development mode', 'ds' ),
		'description' => __( 'Should the development mode be enabled.', 'ds' ),
	] );
} );

add_action( 'admin_menu', function (): void {
	remove_menu_page( 'edit.php?post_type=page' );
	remove_menu_page( 'edit-comments.php' );
} );

add_action( 'template_redirect', function (): void {
	if ( is_singular() ) {
		$terms = get_the_category();
		if ( count( $terms ) > 0 ) {
			if ( wpml_get_current_language() !== wpml_get_default_language() ) {
				wpml_switch_language_action( wpml_get_default_language() );
				$url = get_category_link( apply_filters( 'wpml_object_id', $terms[0]->term_id, 'category' ) );
				wpml_switch_language_action();
			} else {
				$url = get_category_link( $terms[0] );
			}

			$url = add_query_arg( [
				'post' => get_the_ID(),
			], $url );

			wp_redirect( $url );
			exit();
		}
	}
} );

add_filter(
	'allowed_block_types_all',
	function ( $allowed_block_types, WP_Block_Editor_Context $block_editor_context ): array {
		return [
			'core/paragraph',
			'core/heading',
			'core/image',
			'core/gallery',
			'core/audio',
			'core/video',
			'core/buttons',
			'core/button',
			'core/table',
			'core/group',
			'core/columns',
			'core/column',
			'core/reusable',
			'core/separator',
			'core/spacer',
			'ds-blocks/ds-map',
			'ds-blocks/ds-map-panel',
			'ds-blocks/ds-map-tile-toggles',
		];
	},
	10,
	2
);

add_action( 'enqueue_block_editor_assets', function (): void {
	wp_enqueue_script(
		'ds-gutenberg-filters',
		get_template_directory_uri() . '/assets/js/gutenberg-filters.js',
		[ 'wp-edit-post' ]
	);
} );

add_action( 'init', function (): void {
	$regex    = 'ds-api/([^/]*)/?';
	$location = 'index.php?_ds_api_action=$matches[1]';
	$priority = 'top';

	add_rewrite_rule( $regex, $location, $priority );
} );

add_filter( 'query_vars', function ( $vars ): array {
	$vars[] = '_ds_api_action';

	return $vars;
} );

add_filter( 'template_include', function ( $template ): string {
	$action = get_query_var( '_ds_api_action', null );

	if ( $action === 'list' ) {
		$template = __DIR__ . '/api/list.php';
	}

	return $template;
}, 99 );
