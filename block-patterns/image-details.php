<?php

declare(strict_types=1);

$i18n = [
    'description' => 'Beschreibung',
];

if (function_exists('wpml_get_current_language')) {
    if ('fr' === wpml_get_current_language()) {
        $i18n = array_merge($i18n, [
            'description' => 'Description',
        ]);
    }
}

?><!-- wp:columns {"className":"image-details"} -->
<div class="wp-block-columns image-details"><!-- wp:column {"width":"62%"} -->
    <div class="wp-block-column" style="flex-basis:62%"><!-- wp:table -->
        <figure class="wp-block-table">
            <table>
                <tbody>
                <tr>
                    <td>[label-artist]</td>
                    <td>XX</td>
                </tr>
                <tr>
                    <td>[label-object]</td>
                    <td>XX</td>
                </tr>
                <tr>
                    <td>[label-title]</td>
                    <td>XX</td>
                </tr>
                <tr>
                    <td>[label-date]</td>
                    <td>19XX</td>
                </tr>
                <tr>
                    <td>[label-material]</td>
                    <td>XX</td>
                </tr>
                <tr>
                    <td>[label-technique]</td>
                    <td>XX</td>
                </tr>
                <tr>
                    <td>[label-owner]</td>
                    <td>XX</td>
                </tr>
                </tbody>
            </table>
        </figure>
        <!-- /wp:table --></div>
    <!-- /wp:column -->

    <!-- wp:column {"width":"38%"} -->
    <div class="wp-block-column" style="flex-basis:38%">
        <!-- wp:image {"id":18,"sizeSlug":"full","linkDestination":"none"} -->
        <figure class="wp-block-image size-full"><img
                    src="https://dss.nmbiel.ch/wp-content/uploads/2021/10/Beispiel-01.jpg" alt="" class="wp-image-18"/>
        </figure>
        <!-- /wp:image --></div>
    <!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:paragraph -->
<p><?php echo esc_html($i18n['description']); ?></p>
<!-- /wp:paragraph -->
