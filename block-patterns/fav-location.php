<?php

declare( strict_types=1 );

$i18n = [
	'description' => 'Beschreibung',
];

if ( function_exists( 'wpml_get_current_language' ) ) {
	if ( 'fr' === wpml_get_current_language() ) {
		$i18n = array_merge( $i18n, [
			'description' => 'Description',
		] );
	}
}

?>
<!-- wp:heading -->
<h2>Titel</h2>
<!-- /wp:heading -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column -->
    <div class="wp-block-column"><!-- wp:paragraph -->
        <p>Text</p>
        <!-- /wp:paragraph -->

        <!-- wp:image {"align":"left","id":5385,"width":80,"height":105,"sizeSlug":"full","linkDestination":"none","className":"qr-code"} -->
        <figure class="wp-block-image alignleft size-full is-resized qr-code"><img src="https://dss.nmbiel.ch/wp-content/uploads/2022/09/001_3.png" alt="" class="wp-image-5385" width="80" height="105"/><figcaption>SCAN ME</figcaption></figure>
        <!-- /wp:image --></div>
    <!-- /wp:column -->

    <!-- wp:column -->
    <div class="wp-block-column"><!-- wp:image {"id":5381,"sizeSlug":"large","linkDestination":"media"} -->
        <figure class="wp-block-image size-large"><a href="https://dss.nmbiel.ch/wp-content/uploads/2022/09/001_1-1.jpg"><img src="https://dss.nmbiel.ch/wp-content/uploads/2022/09/001_1-1-1024x664.jpg" alt="" class="wp-image-5381"/></a></figure>
        <!-- /wp:image --></div>
    <!-- /wp:column -->

    <!-- wp:column -->
    <div class="wp-block-column"><!-- wp:image {"id":5383,"sizeSlug":"large","linkDestination":"media"} -->
        <figure class="wp-block-image size-large"><a href="https://dss.nmbiel.ch/wp-content/uploads/2022/09/001_2.jpg"><img src="https://dss.nmbiel.ch/wp-content/uploads/2022/09/001_2-1024x664.jpg" alt="" class="wp-image-5383"/></a></figure>
        <!-- /wp:image --></div>
    <!-- /wp:column --></div>
<!-- /wp:columns -->