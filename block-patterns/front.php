<?php

declare(strict_types=1);

$i18n = [
    'title' => 'Titel',
    'introduction' => 'Einleitung',
];

if (function_exists('wpml_get_current_language')) {
    if ('fr' === wpml_get_current_language()) {
        $i18n = array_merge($i18n, [
            'title' => 'Titre',
            'introduction' => 'Introduction',
        ]);
    }
}

?><!-- wp:heading -->
<h2><?php echo esc_html($i18n['title']); ?></h2>
<!-- /wp:heading -->

<!-- wp:group {"className":"image-grid"} -->
<div class="wp-block-group image-grid"><!-- wp:buttons -->
    <div class="wp-block-buttons"><!-- wp:button -->
        <div class="wp-block-button"><a class="wp-block-button__link"
                                        href=""><?php echo esc_html($i18n['introduction']); ?></a></div>
        <!-- /wp:button --></div>
    <!-- /wp:buttons -->

    <!-- wp:image {"id":18,"sizeSlug":"full","linkDestination":"none"} -->
    <figure class="wp-block-image size-full"><img
                src="https://dss.nmbiel.ch/wp-content/uploads/2021/10/Beispiel-01.jpg" alt="" class="wp-image-18"/>
    </figure>
    <!-- /wp:image --></div>
<!-- /wp:group -->
