<?php

declare(strict_types=1);

$i18n = [
    'title' => 'Titel',
];

if (function_exists('wpml_get_current_language')) {
    if ('fr' === wpml_get_current_language()) {
        $i18n = array_merge($i18n, [
            'title' => 'Titre',
        ]);
    }
}

?><!-- wp:heading -->
<h2><?php echo esc_html($i18n['title']); ?></h2>
<!-- /wp:heading -->

<!-- wp:group {"className":"image-grid"} -->
<div class="wp-block-group image-grid"><?php
    for ($i = 0;
    $i < 2;
    $i++) {
    ?><!-- wp:video {"id":45} -->
    <figure class="wp-block-video">
        <video controls poster="https://dss.nmbiel.ch/wp-content/uploads/2021/11/Vorschaubild-1600-900.jpg"
               src="https://dss.nmbiel.ch/wp-content/uploads/2021/11/test.webm"></video>
    </figure>
    <!-- /wp:video --><?php
    }
    ?></div>
<!-- /wp:group -->
