<?php

declare(strict_types=1);

$i18n = [
    'title' => 'Titel',
    'introduction' => 'Einleitungstext',
];

if (function_exists('wpml_get_current_language')) {
    if ('fr' === wpml_get_current_language()) {
        $i18n = array_merge($i18n, [
            'title' => 'Titre',
            'introduction' => 'Introduction',
        ]);
    }
}

?>
<!-- wp:heading -->
<h2><?php echo esc_html($i18n['title']); ?></h2>
<!-- /wp:heading -->

<!-- wp:group {"className":"large"} -->
<div class="wp-block-group large"><!-- wp:paragraph -->
    <p><?php echo esc_html($i18n['introduction']); ?></p>
    <!-- /wp:paragraph --></div>
<!-- /wp:group -->
