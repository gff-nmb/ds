module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  const package_name = 'ds';

  const config = {
    compress: {
      main: {
        options: {
          archive: function () {
            return '../' + package_name + '.zip';
          },
          mode: 'zip',
        },
        files: [
          {
            expand: true,
            cwd: '../',
            src: [
              '**',
              '!_dev/**',
              '!.gitignore',
              '!.gitlab-ci.yml',
              '!' + package_name + '.zip',
              '!tmp/**',
              '!README.md',
            ],
            dest: package_name + '/'
          }
        ]
      }
    }
    ,
    babel: {
      options: {
        sourceMap: false,
        presets: ['@babel/preset-env'],
      },
      dist: {
        files: {
          '../assets/js/gutenberg-filters.js': 'js/gutenberg-filters.js',
        },
      },
    }
    ,
    uglify: {
      options: { mangle: false, compress: false }
    }
    ,
    'dart-sass': {
      options: { trace: false, sourcemap: 'none', style: 'compressed' }
    }
    ,
    cssmin: {
      options: { mergeIntoShorthands: false, roundingPrecision: -1 }
    }
    ,
    postcss: {
      options: { processors: [require('autoprefixer')] }
    }
    ,
    watch: {
      options: { spawn: false },
      jsBabel: {
        files: ['js/gutenberg-filters.js'],
        tasks: ['babel']
      },
      sassAll: {
        files: ['scss/**/*.scss'],
        tasks: ['dart-sass', 'cssmin', 'postcss']
      }
    }
  };

  //////////////////////////////////////////////////////////////////////////////

  /**
   * Adds all configurations needed to build a packed js file.
   *
   * @param {Object} config
   * @param {string} name
   * @param {string} target_path
   * @param {Array} js_files
   * @param {?Object} options
   */
  const add_js_package = (config, name, target_path, js_files, options = undefined) => {
    let uglify = { 'files': {} };
    uglify['files'][target_path] = js_files;

    if (options !== undefined) {
      uglify['options'] = options;
    }

    config['uglify'][name] = uglify;
    config['watch'][name] = { 'files': js_files, tasks: ['uglify:' + name] };
  };

  /**
   * Adds all configurations needed to build a packed css file. Note that the
   * given `css_files` would be appended to the compiled `sass` file.
   *
   * @param {Object} config
   * @param {string} name
   * @param {string} target_path
   * @param {string} sass_src
   * @param {Array} css_files
   */
  const add_css_package = (config, name, target_path, sass_src, css_files = []) => {
    let tmpPath1 = 'tmp/' + name + '.css';
    let tmpPath2 = 'tmp/' + name + '.complete.css';

    let sass = { 'files': {} };
    sass['files'][tmpPath1] = sass_src;
    css_files.unshift(tmpPath1);

    let cssmin = { 'files': {} };
    cssmin['files'][tmpPath2] = css_files;

    let postcss = { 'files': {} };
    postcss['files'][target_path] = tmpPath2;

    config['dart-sass'][name] = sass;
    config['cssmin'][name] = cssmin;
    config['postcss'][name] = postcss;
  };

  /**
   * Adds all configurations needed to build a packed css file for a specific device.
   *
   * @param {Object} config
   * @param {string} name
   */
  const add_device = (config, name) => {
    config['dart-sass']['css-' + name] = {
      files: [{ expand: true, cwd: 'scss/' + name, src: name + '.scss', dest: 'tmp', ext: '.css' }]
    };

    let files = {};
    files['tmp/' + name + '.complete.css'] = ['tmp/' + name + '.css'];
    config['cssmin']['css-' + name] = { files: files };

    let files2 = {};
    files2['../devices/' + name + '.css'] = 'tmp/' + name + '.complete.css';
    config['postcss']['css-' + name] = { files: files2 };
  };

  //////////////////////////////////////////////////////////////////////////////

  add_js_package(config, 'jsMain', '../script.js', [
    'node_modules/@fancyapps/ui/dist/fancybox.umd.js',
    'node_modules/@fancyapps/ui/dist/carousel.autoplay.umd.js',
    'node_modules/overlayscrollbars/js/jquery.overlayScrollbars.js',
    'node_modules/swiper/swiper-bundle.min.js',
    'js/watchdog.js',
    'js/controller.js',
    'js/autoReload.js',
    'js/fancybox.js',
    'js/swiper.js',
    'js/flip-book.js',
    'js/audio.js',
    'js/video.js',
    'js/script.js'
  ]);

  add_js_package(config, 'jsTestRunner', '../test/runner.js', ['js/test/runner.js']);

  add_js_package(config, 'jsTestScreen', '../test/screen.js', ['js/test/screen.js']);

  ///
  // Museum Map.
  ///
  add_js_package(config, 'jsMuseumMap', '../assets/museum-map/museum-map.js', ['js/museum-map.js'], {
    mangle: true,
    compress: true
  });

  add_css_package(config, 'cssMuseumMap', '../assets/museum-map/museum-map.css', 'scss/museum-map.scss');

  add_js_package(config, 'jsMuseumMapOption', '../assets/museum-map/museum-map-option.js', ['js/museum-map/museum-map-option.js'], {
    mangle: true,
    compress: true
  });

  add_css_package(config, 'cssMuseumMapOption', '../assets/museum-map/museum-map-option.css', 'scss/museum-map/option/museum-map-option.scss');

  ///
  // Touch Animation.
  ///
  add_js_package(config, 'jsTouchAnimation', '../touch-animation.js', ['js/touch-animation.js'], {
    mangle: true,
    compress: true
  });

  add_css_package(config, 'cssMain', '../style.css', 'scss/style.scss', [
    'node_modules/@fancyapps/ui/dist/fancybox.css',
    'node_modules/overlayscrollbars/css/OverlayScrollbars.css',
  ]);

  add_css_package(config, 'cssEditor', '../editor-style.css', 'scss/editor-style.scss');

  add_css_package(config, 'cssTouchAnimation', '../touch-animation.css', 'scss/touch-animation.scss');

  add_device(config, 'galaxy-tab-a7-10-40');
  add_device(config, 'xoro-megapad-1405');
  add_device(config, 'xoro-megapad-2404');
  add_device(config, 'xoro-megapad-3204');
  add_device(config, 'pm43f');

  // noinspection JSUnresolvedFunction
  grunt.initConfig(config);

  // Register the tasks.
  grunt.registerTask('default', ['babel', 'uglify', 'dart-sass', 'cssmin', 'postcss', 'watch']);
  grunt.registerTask('build', ['compress']);
};
