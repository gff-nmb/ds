const dsFlipBook = ((win, doc, $, _watchdog, _controller) => {
  'use strict';

  const flip_book_names = ['must-see', 'real-biel', 'secret-spot'];

  let _flip_book_selectors = '';
  flip_book_names.forEach((item) => {
    if (_flip_book_selectors.length > 0) {
      _flip_book_selectors += ',';
    }
    _flip_book_selectors += '.flip-book-' + item;
  });

  let interval = undefined;

  const _flip_book_interval_handler = () => {

    $flip_books.each(function () {
      const $flip_book = $(this);

      const $visible_figure = $flip_book.find('figure:visible');

      const nrFigures = $flip_book.find('figure').length;
      let index = $visible_figure.index() + 1;
      if (index >= nrFigures) {index = 0;}

      $visible_figure.hide();
      $($flip_book.find('figure').get(index)).show();
    });
  };

  let $flip_books = null;

  const _attempt_start = () => {
    $flip_books = $(_controller.getCurrentContainer()).find(_flip_book_selectors);
    if ($flip_books.length > 0) {
      // The current container (usually the home container) does have flip books.
      $flip_books.each(function () {
        const $flip_book = $(this);

        // Hide the currently visible figure.
        $flip_book.find('figure:visible').hide();

        // Get the number of figures and select randomly which one should be visible.
        const nrFigures = $flip_book.find('figure').length;
        const index = Math.floor(Math.random() * nrFigures);

        // Show the one.
        $($flip_book.find('figure').get(index)).show();
      });

      interval = win.setInterval(_flip_book_interval_handler, 500);
    }
  };

  const _init = ($body) => {

    const $flip_books = $body.find(_flip_book_selectors);
    if ($flip_books.length > 0) {

      $flip_books.each(function () {
          const $flip_book = $(this);

          flip_book_names.forEach((item) => {
            if ($flip_book.hasClass('flip-book-' + item)) {
              $flip_book.find('.wp-block-group__inner-container').append($('#flip-book-passepartout-' + item + ' > div').clone());
            }
          });

          $flip_book.find('figure').first().show();
        }
      );

      $body.on('ds-enter', function (event) {
        // If the current post container contains a flip-book (or many), we will start the interval.
        _attempt_start();
      });

      $body.on('ds-leave', function (e) {
        // We will stop the interval if set.
        if (interval !== undefined) {
          win.clearInterval(interval);
          interval = undefined;
        }
      });
    }

    _attempt_start();
  };

  return {
    init: _init
  };
})
(window, document, jQuery, dsWatchdog, dsController);