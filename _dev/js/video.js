const dsVideo = ((doc, ds, $, _dsWatchdog, _dsController, _dsAutoReload) => {
  'use strict';

  // Private variables.
  let _$body, _$videoModal, _$overlay;

  // Initialize the `videoPlayer` variable. In case of success, this variable
  // would hold the `MediaElement` instance shown on the video modal.
  let _videoPlayer;

  const _init = ($body, $overlay, screenLayout) => {
    _$body = $body;
    _$overlay = $overlay;

    // Convert all video elements into a preview image, attach click event
    // listeners on the preview images, show the video player once clicked.
    $('video').each(function () {
      let $video = $(this);

      // If we are in screen mode 1, then the video should not be replaced by the preview image.
      if (screenLayout === 1) {
        // Note that in order to autoplay videos non-muted, the browser needs to be allowed to do so (by the kiosk user).
        this.muted = false;

        $video.mediaelementplayer({
          features: [],
          clickToPlayPause: false,
          stretching: 'responsive',
          success: function (mediaElement, originalNode, instance) {
            if (!$video.closest(_dsController.getCurrentContainer()).length) {
              // This video is not on the current container, so pause it.
              mediaElement.pause();
            }

            let $mejsContainer = $(originalNode).closest('.mejs-container');
            $mejsContainer.find('.mejs-controls').css('backgroundColor', 'transparent');
          }
        });

        return;
      }

      let posterSource = $video.attr('poster');
      if (posterSource === undefined) {
        posterSource = ds['video']['poster_default'];
      }

      let videoSource = $video.attr('src');
      let videoMuted = ($video.attr('muted') !== undefined);
      $video.replaceWith('<a href="#" data-action="play-video" data-video="' + videoSource + '" data-muted="' + videoMuted + '"><div class="icon-button-overlay">' + ds['video']['play_button'] + '</div><img src="' + posterSource + '" alt=""></a>');
    });

    // Prepend the video modal to the body.
    $body.prepend('<div id="video-modal" style="display:none"><video></video></div>');
    _$videoModal = $('#video-modal');

    if ($body.hasClass('autoplay-screen')) {
      ///
      // Special treatment if that screen is an autoplay screen.
      ///

      // Get the first video on the current container (which should be the home container)
      // and extract the video url.
      const videoUrl = $(_dsController.getCurrentContainer()).find('[data-action="play-video"]').data('video');

      _$videoModal.find('video').mediaelementplayer({
        features: ['current', 'duration'],
        stretching: 'fill',
        clickToPlayPause: false,
        alwaysShowControls: true,
        loop: true,
        success: function (mediaElement, originalNode, instance) {
          _videoPlayer = instance;

          _videoPlayer.setSrc(videoUrl);

          // noinspection JSUnresolvedFunction
          _videoPlayer.setMuted(true);

          _$overlay.show();
          _$videoModal.show();

          _videoPlayer.play();
        }
      });
    } else {
      _$videoModal.find('video').mediaelementplayer({
        features: ['playpause', 'current', 'duration', 'volume'],
        stretching: 'fill',
        success: function (mediaElement, originalNode, instance) {
          _videoPlayer = instance;

          let $mejsContainer = $(originalNode).closest('.mejs-container');
          $mejsContainer.find('.mejs-overlay-play').prepend('<div class="mejs-close-button">' + ds.video['close_button'] + '</div>');
          $mejsContainer.find('.mejs-overlay-play .mejs-overlay-button').html(ds.video['play_button']);

          $mejsContainer.find('.mejs-controls .mejs-playpause-button').before('<div class="mejs-button">' + ds.video['close_button'] + '</div>');
          $mejsContainer.find('.mejs-controls .mejs-playpause-button')
            .html('<div class="play-button">' + ds.video['play_button'] + '</div><div class="pause-button">' + ds.video['pause_button'] + '</div>');

          mediaElement.addEventListener('ended', function () {
            _closeVideo();
          });
        }
      });
    }
  };

  const _closeVideo = () => {
    _$videoModal.hide();
    _$overlay.hide();

    if (_videoPlayer === null) {
      return;
    }

    if (!_videoPlayer.paused) {
      _videoPlayer.pause();
      _videoPlayer.setCurrentTime(0);
    }

    _dsAutoReload.enableSoftReload();
  };

  /**
   * Starts playing the video in the mediaelement player in the modal box.
   *
   * @param videoSource {string}
   * @param videoMuted {bool}
   */
  const _playVideo = (videoSource, videoMuted) => {
    if (_videoPlayer === null) {
      return;
    }

    _videoPlayer.setSrc(videoSource);
    _videoPlayer.setMuted(videoMuted);

    if (videoMuted) {
      $('#video-modal').find('.mejs-volume-button').hide();
    } else {
      $('#video-modal').find('.mejs-volume-button').show();
      _videoPlayer.setVolume(0.75);
    }

    _$overlay.show();
    _$videoModal.show();
    _videoPlayer.play();

    _dsAutoReload.disableSoftReload();
  };

  return {
    init: _init,
    playVideo: _playVideo,
    closeVideo: _closeVideo
  };
})(document, window.ds, jQuery, dsWatchdog, dsController, dsAutoReload);
