const dsWatchdog = ((doc) => {
  'use strict';

  /**
   * Watchdog logs.
   *
   * @type {[{date: Date, name: string, data: Object|null}]}
   */
  const logs = [];

  /**
   * Adds a new watchdog record.
   *
   * @param name {string}
   * @param data {Object|null}
   */
  const _add = (name, data = null) => {
    _logsAdd(new Date(), name, data);
    _logsClean(100);
  };

  /**
   * Adds a new log.
   *
   * @param date {Date}
   * @param name {string}
   * @param data {Object|null}
   *
   * @private
   */
  const _logsAdd = (date, name, data = null) => {
    logs.push({
      date: date,
      name: name,
      data: data
    });
  };

  /**
   * Cleans the logs (assures a maximum size).
   *
   * @param maximum {number}
   *
   * @private
   */
  const _logsClean = (maximum) => {
    if (logs.length > maximum) {
      logs.splice(0, logs.length - maximum);
    }
  };

  /**
   * Resets the logs (removes all logs).
   *
   * @private
   */
  const _logsReset = () => {
    logs.splice(0);
  };

  /**
   * Initializes the watchdog.
   */
  const _init = () => {
    _add('watchdog.init');

    // If there is a parent window (the window this site is embedded as iframe)
    // post a message containing the watchdog logs every 5 seconds.
    setInterval(function () {
      if (window.parent === undefined) {
        return;
      }

      window.parent.postMessage({
        'type': 'watchdog',
        'logs': logs,
      }, '*');
    }, 5000);

    doc.addEventListener('ds-action', evt => {
      let data = evt.detail;

      const name = data['name'];
      delete data['name'];

      _add(name, data);
    });
  };

  return {
    init: _init,
    add: _add,
    /**
     * Resets the watchdog logs.
     */
    reset: () => {
      _logsReset();
    },
    /**
     * Gets the watchdog logs.
     *
     * @return {{date: Date, name: string, data: (Object|null)}[]}
     */
    get: () => {
      return logs;
    }
  };
})(document);
