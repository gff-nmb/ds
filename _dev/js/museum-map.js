/**
 * Note: This file would only be loaded if the shortcode had been used.
 */
(function (win, doc, jQuery) {
  'use strict';

  jQuery(function ($) {
    const $body = $('body');

    const container_selector = '.ds-museum-map-container';
    const data_ds_museum_map_toggle = 'ds-museum-map-toggle';
    const data_ds_museum_map_group = 'ds-museum-map-group';
    const class_visible = 'visible';

    const str_data = 'data';
    const str_background_color = 'background-color';

    /**
     * Reset the museum map(s) inside the given museum map container(s).
     *
     * @param $museum_map_container
     */
    const museum_map_reset = function ($museum_map_container) {
      $museum_map_container.find('[' + str_data + '-' + data_ds_museum_map_toggle + ']').css(str_background_color, '');
      $museum_map_container.find('[' + str_data + '-' + data_ds_museum_map_group + ']').removeClass(class_visible);
    };

    ///
    // Initialize all museum map(s).
    ///
    $body.on('click', '[' + str_data + '-' + data_ds_museum_map_toggle + ']', function () {
      const $toggle_button = $(this);

      const $mm_container = $toggle_button.closest(container_selector);
      museum_map_reset($mm_container);

      $toggle_button.css(str_background_color, $toggle_button.data('ds-museum-map-color'));
      $mm_container.find('[' + str_data + '-' + data_ds_museum_map_group + '=' + $toggle_button.data(data_ds_museum_map_toggle) + ']').addClass(class_visible);
    });

    $body.on('ds-leave', '.post-container', evt => {
      const $post_container = $(evt.currentTarget);

      museum_map_reset($post_container.find(container_selector));
    });
  });
})(window, document, jQuery);
