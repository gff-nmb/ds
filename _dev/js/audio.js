const dsAudio = ((win, doc, $, _ds_watchdog) => {
  'use strict';

  const _init = ($body) => {
    const $audio = $('audio');
    $audio.attr('height', $audio.height() + 'px');

    $audio.mediaelementplayer({
      features: ['playpause', 'current', 'progress', 'duration', 'volume'],
      audioWidth: '100%',
      success: function (mediaElement, originalNode, instance) {
        mediaElement.addEventListener('play', function () {
          _ds_watchdog.add('Play audio.', {
            'src': instance.src
          });
        });
      },
    });
  };

  return {
    init: _init
  };
})(window, document, jQuery, dsWatchdog);
