const dsController = ((doc) => {
  'use strict';

  /**
   * Gets the current container or `null` if there is no current container. Note
   * that this function actually returns the first post container that has the class "active".
   *
   * @returns {Element|null}
   */
  const _getCurrentContainer = function () {
    return doc.querySelector('.post-container.active');
  };

  /**
   * Gets the current language or `null` if anything fails.
   *
   * @returns {string|null}
   */
  const _getCurrentLanguage = function () {
    const c = _getCurrentContainer();
    return (c === null) ? null : c.getAttribute('data-lang');
  };

  /**
   * Gets the current poly id or `null` if anything fails.
   *
   * @return {string|null}
   */
  const _getCurrentPoly = function () {
    const c = _getCurrentContainer();
    return (c === null) ? null : c.getAttribute('data-poly');
  };

  /**
   * Gets the current permalink or `null` if anything fails.
   *
   * @returns {string|null}
   */
  const _getCurrentPermalink = function () {
    const c = _getCurrentContainer();
    return (c === null) ? null : c.getAttribute('data-permalink');
  };

  /**
   * Gets the home poly id or `null` if anything fails.
   *
   * @return {string|null}
   */
  const _getHomePoly = function () {
    const c = doc.querySelector('[data-home]');
    return (c === null) ? null : c.getAttribute('data-poly');
  };

  /**
   * Gets the container given the poly id and the language.
   *
   * @param {string} polyId
   * @param {string} lang
   *
   * @returns {Element|null}
   */
  const _getContainer = function (polyId, lang) {
    return doc.querySelector('[data-lang="' + lang + '"][data-poly="' + polyId + '"]');
  };

  /**
   * Is the current poly the home poly?
   *
   * @returns {boolean}
   */
  const _isHomePoly = function () {
    return _getHomePoly() === _getCurrentPoly();
  };

  return {
    getCurrentContainer: _getCurrentContainer,
    getCurrentLanguage: _getCurrentLanguage,
    getCurrentPoly: _getCurrentPoly,
    getCurrentPermalink: _getCurrentPermalink,
    getHomePoly: _getHomePoly,
    getContainer: _getContainer,
    isHomePoly: _isHomePoly
  };
})(document);
