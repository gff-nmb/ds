const dsFancybox = ((doc, $, _ds_watchdog, _ds_controller) => {
  'use strict';

  const _init = () => {
    // Set fancybox defaults.
    Fancybox.defaults.Hash = false;

    const _event_closing = (fancybox) => {
      const eventData = {
        'name': 'fancybox.closing',
        'id': fancybox.id
      };

      doc.dispatchEvent(new CustomEvent('ds-action', { detail: eventData }));
    };

    let _event_carousel_select_slide = (fancybox, carousel, slide) => {
      const eventData = {
        'name': 'fancybox.select',
        'id': fancybox.id,
        'slide': slide.index,
        'src': slide.src
      };

      doc.dispatchEvent(new CustomEvent('ds-action', { detail: eventData }));
    };

    /**
     * Gets the file extension (lowercase) given a url.
     *
     * @param {string} url
     * @returns {string}
     * @private
     */
    const _get_url_extension = function (url) {
      return url.split(/[#?]/)[0].split('.').pop().trim().toLowerCase();
    };

    /**
     * No-operation function.
     *
     * @private
     */
    const _noop = () => {};

    /**
     * Handler for the `closing` event of a fancybox.
     *
     * @param fancybox
     * @private
     */
    const _fancybox_on_closing = (fancybox) => {
      _event_closing(fancybox);
    };

    /**
     * Handler for the `select slide` event of a fancybox carousel.
     *
     * @param {Object} fancybox
     * @param {Object} carousel
     * @param {Object} slide
     * @private
     */
    const _fancybox_carousel_on_select_slide = (fancybox, carousel, slide) => {
      _event_carousel_select_slide(fancybox, carousel, slide);
    };

    /**
     * Bind the standard fancybox.
     *
     * @param {string} selector
     * @private
     */
    const _bind_standard_fancybox = function (selector) {
      Fancybox.bind(selector, {
        click: _noop,
        Toolbar: {
          display: [
            { id: 'prev', position: 'center' },
            { id: 'counter', position: 'center' },
            { id: 'next', position: 'center' },
            'zoom',
            'close'
          ]
        },
        on: {
          'closing': _fancybox_on_closing,
          'Carousel.selectSlide': _fancybox_carousel_on_select_slide
        }
      });
    };

    ///
    // Handle the special case of autoplay-gallery-screen.
    //   All galleries should open in fullscreen mode (without close button).
    //   The first gallery on the home screen should automatically open.
    //   => Fancybox.fromOpener(the_first_anchor_element);
    ///
    if ($('body').hasClass('autoplay-gallery-screen')) {
      // Get the home container (in the current language).
      const homeContainer = _ds_controller.getContainer(
        _ds_controller.getHomePoly(),
        _ds_controller.getCurrentLanguage()
      );

      // Find the first gallery block and iterate over the a-tags to build the images array.
      let images = [];
      $(homeContainer).find('.wp-block-gallery').first().find('a[href]').each(function () {
        images.push({
          src: $(this).attr('href'),
          type: 'image'
        });
      });

      // Show the fancybox.
      Fancybox.show(images, {
        animated: false,
        dragToClose: false,
        infinite: true,

        showClass: false,
        hideClass: false,

        closeButton: 'top',
        click: function () {},

        Toolbar: false,
        Thumbs: false,
        Image: {
          click: null,
          wheel: 'slide',
          zoom: false,
          fit: 'contain'
        },
        Carousel: {
          friction: 0.96,
          Autoplay: {
            timeout: 5000
          }
        },

        on: {
          'closing': _fancybox_on_closing,
          'Carousel.selectSlide': _fancybox_carousel_on_select_slide
        }
      });

      return;
    }

    ///
    // Handle the default case.
    ///
    let ctr = 0;

    ///
    // Single images.
    ///
    const single_image_selector = '.wp-block-image a';
    doc.querySelectorAll(single_image_selector).forEach(element => {
      const href = element.getAttribute('href');
      if (href !== undefined) {
        const extension = _get_url_extension(href);
        if (extension === 'jpg' || extension === 'jpeg' || extension === 'gif' || extension === 'png' || extension === 'webp') {
          element.setAttribute('data-fancybox', 'fancybox-' + (ctr++));
        }
      }
    });

    // Bind the fancybox library.
    _bind_standard_fancybox(single_image_selector + '[data-fancybox]');

    ///
    // Galleries.
    ///
    doc.querySelectorAll('.wp-block-gallery').forEach(element => {
      ctr++;

      element.querySelectorAll('a').forEach(a => {
        a.setAttribute('data-fancybox', 'fancybox-' + ctr);
      });
    });

    // Bind the fancybox library.
    _bind_standard_fancybox('.wp-block-gallery a');
  };

  return {
    init: _init
  };
})(document, jQuery, dsWatchdog, dsController);
