const dsAutoReload = ((win, _dsController) => {
  'use strict';

  let _lastInteractionTime = Date.now();
  let _reloadEnabled = true;
  let _softReloadEnabled = true;
  let _timeInterval = 5000;

  const _resetIdleTimer = () => {
    _lastInteractionTime = Date.now();
  };

  const _checkReload = () => {
    const diff = Date.now() - _lastInteractionTime;

    if (_reloadEnabled && !_dsController.isHomePoly()) {
      if (_softReloadEnabled && diff >= 180000 /* 3 minutes */) {
        // Soft Reload.
        win.location.reload();
        return;
      } else if (diff > 900000 /* 15 minutes */) {
        // Hard Reload.
        win.location.reload();
        return;
      }
    }

    win.setTimeout(_checkReload, _timeInterval);
  };

  /**
   * Initializes the auto-reload.
   *
   * @param $body {jQuery}
   */
  const init = ($body) => {
    _resetIdleTimer();

    $body.on('click keypress', _resetIdleTimer);

    win.setTimeout(_checkReload, _timeInterval);
  };

  return {
    init,
    resetIdleTimer: _resetIdleTimer,
    enableReload: () => { _reloadEnabled = true; },
    disableReload: () => { _reloadEnabled = false; },
    enableSoftReload: () => { _softReloadEnabled = true; },
    disableSoftReload: () => { _softReloadEnabled = false; },
  };
})(window, dsController);
