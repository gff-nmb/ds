(function (win, doc, $, _watchdog, _controller, _auto_reload, _fancybox, _swiper, _flip_book, _audio, _video) {
  'use strict';

  // The `win['ds']` object would be set in `/functions.php` using
  // `wp_localize_script()`.
  const ds = win['ds'];

  win.DS_Theme = {
    /**
     * Initializes overlay scrollbars for the given elements.
     *
     * @param $elements
     */
    os_init: ($elements) => {
      $elements.overlayScrollbars({ className: 'os-theme-ds' });
    }, /**
     * Scrolls the overlay scrollbars for the given elements to the given position.
     *
     * @param $elements
     * @param position
     */
    os_scroll: ($elements, position) => {
      $elements.each(function () {
        const instance = $(this).overlayScrollbars();
        if (instance instanceof OverlayScrollbars) {
          instance.scroll(position);
        }
      });
    }, controller: _controller
  };

  $(() => {
    const __post_container_selector = '.post-container';
    const __post_container_active_class = 'active';
    const __button_active_class = 'active';
    const __action_name = 'action';
    const __action_go_back = 'go-back';
    const __action_switch_language = 'switch-language';

    _watchdog.init();

    /**
     * The languages.
     *
     * @type {string[]}
     */
    const languages = ['de', 'fr'];

    /**
     * The history.
     *
     * @type {*[]}
     */
    const history = [];

    /**
     * The body.
     *
     * @type {jQuery|HTMLElement|*}
     */
    const $body = $('body');

    /**
     * The overlay that would be visible in case a modal (like the video player) is shown.
     *
     * @type {jQuery|HTMLElement|*}
     */
    const $overlay = $('.overlay');

    /**
     * The screen layout (based on the current device).
     * `0`: Default layout (language switcher and back button on the left side).
     * `1`: Bottom navigation bar (language switcher in bottom bar).
     *
     * Note that the behaviour would also adapt.
     *
     * @type {number}
     */
    const screen_layout = ($body.is('.pm43f') ? 1 : 0);

    ///
    // Initialize the auto-reload.
    ///
    _auto_reload.init($body);

    if (screen_layout === 1) {
      _auto_reload.disableReload();
    }

    const data_action = 'data-' + __action_name;
    const go_back_buttons = doc.querySelectorAll('[' + data_action + '="' + __action_go_back + '"]');
    const switch_language_buttons = doc.querySelectorAll('[' + data_action + '="' + __action_switch_language + '"]');

    $body.on('ds-enter', __post_container_selector, evt => {
      let $container = $(evt.currentTarget);

      $container.addClass(__post_container_active_class);

      $container.find('audio[autoplay],video[autoplay]').each(function () {
        this.currentTime = 0;
        this.play();
      });
    });

    $body.on('ds-leave', __post_container_selector, evt => {
      const $post_container = $(evt.currentTarget);

      $post_container.find('audio,video').each(function () {
        this.pause();
        this.currentTime = 0;

        if (this.tagName === 'AUDIO') {
          this.muted = false;
          this.volume = 1;
        }
      });

      win.DS_Theme.os_scroll($post_container, 0);
      $post_container.removeClass(__post_container_active_class);
    });

    /**
     * Updates the panels (i.e. the icon buttons).
     */
    const update_panels = function () {
      const language = _controller.getCurrentLanguage();
      const poly = _controller.getCurrentPoly();

      if (screen_layout !== 1) {
        const go_back_display = (history.length === 0) ? 'none' : '';
        go_back_buttons.forEach(element => {
          element.style.display = go_back_display;
        });
      }

      const language_display = (!_controller.isHomePoly() && screen_layout !== 1) ? 'none' : '';
      switch_language_buttons.forEach(element => {
        element.style.display = language_display;
      });

      if (language_display !== 'none') {
        $('[data-' + __action_name + '="' + __action_switch_language + '"]').each(function () {
          const $t = $(this);
          const targetLanguage = $t.data('lang');

          // Show/Hide language selector whether the container actually exists.
          $t.toggle(_controller.getContainer(poly, targetLanguage) !== null);

          $t.toggleClass(__button_active_class, targetLanguage === language);
        });
      }

      if (screen_layout === 1) {
        // Update the navigation bar.
        const permalink = _controller.getCurrentPermalink();

        $('.navigation').each(function () {
          const $navigation = $(this);
          $navigation.toggle($navigation.data('lang') === language);
          $navigation.find('a[href]').each(function () {
            const $a = $(this);
            $a.toggleClass(__button_active_class, $a.attr('href') === permalink);
          });
        });
      }
    };

    /**
     * Goes to the given container and calls the callback if the container could be found.
     *
     * @param {string} poly_id
     * @param {string} language
     * @param {Function} callback
     */
    const _go_to = function (poly_id, language, callback) {
      const goto_container = _controller.getContainer(poly_id, language);
      if (goto_container === null) {
        console.error('The container «' + poly_id + ', ' + language + '» could not be found.');
        return;
      }

      callback();

      _watchdog.add('goTo', { 'poly': parseInt(poly_id), 'lang': language });

      const current_container = _controller.getCurrentContainer();
      if (current_container !== null) {
        current_container.dispatchEvent(new CustomEvent('ds-leave', { bubbles: true }));
      }

      goto_container.dispatchEvent(new CustomEvent('ds-enter', { bubbles: true }));

      update_panels();
    };

    /**
     * Pushes a new history item.
     *
     * @param {string} poly_id
     * @param {string} language
     */
    const _history_add = (poly_id, language) => {
      history.push({ id: poly_id, lang: language });
    };

    /**
     * Limits the history.
     */
    const _history_limit = () => {
      if (history.length > 100) {
        history.splice(0, history.length - 100);
      }
    };

    /**
     * Switches the language.
     *
     * @param {string} language
     */
    const switch_language = function (language) {
      const current_poly_id = _controller.getCurrentPoly();
      const current_language = _controller.getCurrentLanguage();

      if (language === current_language) {
        console.debug('Do not switch language as we are already in that language :-).');
        return;
      }

      _go_to(current_poly_id, language, function () {
        // We push the current container (inclusive language) into the history,
        // except if the history is empty, i.e. we are on the home container.
        if (history.length > 0) {
          _history_add(current_poly_id, current_language);
          _history_limit();
        }
      });
    };

    /**
     * Goes to the container given by poly id and language.
     *
     * @param {string} poly_id
     * @param {string} language
     */
    const go_to = function (poly_id, language) {
      const current_poly_id = _controller.getCurrentPoly();
      const current_language = _controller.getCurrentLanguage();

      _go_to(poly_id, language, function () {
        _history_add(current_poly_id, current_language);
        _history_limit();
      });
    };

    /**
     * Goes back in history.
     */
    const go_back = function () {
      if (history.length === 0) {
        return;
      }

      const last_history_item = history[history.length - 1];
      _go_to(last_history_item['id'], last_history_item['lang'], function () {
        history.pop();
      });
    };

    //////////////////////////////////////////////////////////////////////////////
    // If there is a `post` GET parameter, then try to go to the corresponding post.
    const url_params = new URLSearchParams(win.location.search);
    if (url_params.has('post')) {
      const $post_container = $('#post-container-' + parseInt(url_params.get('post')));
      if ($post_container.length === 1) {
        go_to($post_container.data('poly'), $post_container.data('lang'));
      }
    } else {
      // Dispatch a `ds-enter` event on the current post container.
      const current_container = _controller.getCurrentContainer();
      if (current_container !== null) {
        current_container.dispatchEvent(new CustomEvent('ds-enter', { bubbles: true }));
      }
    }
    //////////////////////////////////////////////////////////////////////////////

    // Initialize the navigation in case of screen layout 1 (bottom navigation).
    if (screen_layout === 1) {
      for (let i = 0; i < languages.length; i++) {
        const lang = languages[i];

        let navigation = '';
        $(__post_container_selector + '[data-lang="' + lang + '"]').each(function () {
          const $post_container = $(this);

          let title = $post_container.find('article').data('title');
          // Strip the parts before the first '-'.
          let p0 = title.indexOf('–');
          if (p0 >= 0) {
            title = title.substring(p0 + 1).trim();
          }
          // Remove the leading "FR " if existent.
          if (title.substring(0, 3) === 'FR ') {
            title = title.substring(3).trim();
          }

          navigation = '<li><a href="' + $post_container.data('permalink') + '">' + title + '</a></li>' + navigation;
        });

        navigation = '<div class="navigation" data-lang="' + lang + '"><ul>' + navigation + '</ul></div>';
        $('.page-sidebar-top').append(navigation);
      }
    }

    //////////////////////////////////////////////////////////////////////////////
    update_panels();
    //////////////////////////////////////////////////////////////////////////////
    doc.oncontextmenu = function () {
      return false;
    };
    //////////////////////////////////////////////////////////////////////////////
    doc.querySelectorAll('a').forEach(element => {
      const href = element.getAttribute('href');
      if (href === null) {
        return;
      }

      // Search the `href` in the permalinks of this screen (category).
      const nodes = doc.querySelectorAll('[data-permalink="' + href + '"]');
      if (nodes.length > 0) {
        element.addEventListener('click', event => {
          event.preventDefault();

          go_to(nodes.item(0).getAttribute('data-poly'), nodes.item(0).getAttribute('data-lang'));
        });
      }
    });

    _swiper.init($body);
    _flip_book.init($body);
    _audio.init($body);
    _video.init($body, $overlay, screen_layout);

    $body.on('click', '[data-' + __action_name + ']', function (event) {
      event.preventDefault();

      let $t = $(this);
      let action = $t.data(__action_name);
      switch (action) {
        case __action_switch_language:
          switch_language(event.currentTarget.getAttribute('data-lang'));
          break;

        case __action_go_back:
          go_back();
          break;

        case 'play-video':
          _video.playVideo($t.data('video'), $t.data('muted'));
          break;

        case 'close-video':
          _video.closeVideo();
          break;
      }
    });

    //////////////////////////////////////////////////////////////////////////////
    // Fancybox Setup
    //////////////////////////////////////////////////////////////////////////////
    _fancybox.init();

    if (!$body.hasClass('category-2-3-2')) {
      // We need to check for this category as this category is very special (Neptun movie).
      win.DS_Theme.os_init($(__post_container_selector));
    }

    // Attach a flag to the `body` element, indicating that the screen is initialized.
    // This would mainly be used by the integration test scripts.
    $body.data('init', true);
  });

})(window, document, jQuery, dsWatchdog, dsController, dsAutoReload, dsFancybox, ds_swiper, dsFlipBook, dsAudio, dsVideo);
