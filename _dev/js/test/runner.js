(function (win, $) {
  'use strict';

  $(function () {
    let runAll = false;
    let currentIndex;

    const filterWarning = (message) => {
      message = message.replace(/\[img:(.*)]/, '<span class="hover-image">[Image<span><img src="$1" alt=""></span>]</span>');
      return message;
    };

    win.addEventListener('message', (event) => {
      if (event.origin !== 'https://dss.nmbiel.ch') {
        console.error('Wrong origin of message');
        return;
      }

      let reportHtml = '<ul>';
      if (event.data.warnings.length > 0) {
        for (let i = 0; i < event.data.warnings.length; i++) {
          reportHtml += '<li style="color:#c00;">' + filterWarning(event.data.warnings[i]) + '</li>';
        }
      } else {
        reportHtml += '<li style="color:#0c0;">Test success</li>';
      }
      reportHtml += '</ul>';

      const $link = $($categoryLinks.get(currentIndex));
      const $li = $link.closest('li');
      $li.remove('.report');
      $li.append('<div class="report">' + reportHtml + '</div>');

      if (runAll) {
        testNextCategory();
      } else {
        // Enable buttons again.
        $('button').prop('disabled', false);
      }
    }, false);

    const style = document.createElement('style');
    style.innerHTML = '' +
      'li { display:flex; flex-wrap:wrap; align-items:center; }' +
      '.category-link { min-width:80px; }' +
      '.action-buttons { padding-left:20px; flex-grow:1; display:flex; }' +
      'button { background-color:#333; color:#ccc; border:1px solid #555; }' +
      'button:disabled,button[disabled]{ background-color:#111; color:#666; }' +
      '.report { width:100%; padding:.5em 2em 1em 2em; font-size:0.75em; font-family:monospace; }' +
      '.report ul li { margin-bottom:0.4em; }' +
      '.report li { display:block; }' +
      '.report .hover-image { position:relative; }' +
      '.report .hover-image > span { display:none; }' +
      '.report .hover-image > span img { width:200px; height:auto; }' +
      '.report .hover-image:hover > span { position:absolute; display:block; bottom:1.5em; left:4em; background-color: #e00; padding:2px; z-index:1; }';
    document.head.appendChild(style);

    $('#content').append('<div><button data-action="integration-test">Run all integration tests</button></div><iframe id="iframe" style="position:absolute;left:-2000px;top:-2000px;width:1200px;height:800px;"></iframe>');

    let testNextCategory = function () {
      currentIndex++;

      if (currentIndex >= $categoryLinks.length) {
        runAll = false;
        return;
      }

      const now = new Date();
      const $link = $($categoryLinks.get(currentIndex));
      $link.closest('li').find('.report').remove();
      $('#iframe').attr('src', $link.attr('href') + '?test=integration&ts=' + now.getTime());
    };

    const $categoryLinks = $('.category-link');
    $categoryLinks.each(function () {
      $(this).closest('li').append('<div class="action-buttons"><button data-action="run-single">Run integration test</button></div>');
    });

    const $body = $('body');
    $body.on('click', '[data-action="run-single"]', function () {
      $('button').prop('disabled', true);

      runAll = false;
      currentIndex = $categoryLinks.index($(this).closest('li').find('a.category-link').get(0)) - 1;
      testNextCategory();
    });

    $body.on('click', '[data-action="integration-test"]', function () {
      $('button').prop('disabled', true);

      runAll = true;
      currentIndex = -1;
      testNextCategory();
    });
  });
})(window, jQuery);