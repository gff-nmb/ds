///
// Integration Test
///
(function (win, doc, $) {
  'use strict';

  const hostOrigin = 'https://dss.nmbiel.ch';

  class Report {
    constructor () {
      this.warnings = [];
    }

    addWarning (warning) {
      this.warnings.push(warning);
    }

    send () {
      console.log(this.warnings);

      win.parent.postMessage({
        warnings: this.warnings
      }, hostOrigin);
    }
  }

  class PostContainer {
    /**
     * @type {number}
     */
    #postID = 0;

    /**
     * @type {[PostContainer]}
     */
    #links = [];

    /**
     * @type {string}
     */
    #title = 'No title';

    /**
     * @type {boolean}
     */
    #isHome = false;

    /**
     * @type {string}
     */
    #permalink = '';

    /**
     * @type {jQuery}
     */
    #$node;

    /**
     * @param $node {jQuery}
     */
    constructor ($node) {
      this.poly = $node.data('poly');
      this.lang = $node.data('lang');
      this.#postID = parseInt($node.attr('id').substring(15));
      this.#permalink = $node.data('permalink');
      this.#title = $node.find('article').data('title');
      this.#isHome = ($node.data('home') !== undefined);
      this.#$node = $node;
    }

    /**
     * @return {string}
     */
    get title () {
      return this.#title;
    }

    /**
     * @return {number}
     */
    get postID () {
      return this.#postID;
    }

    /**
     * @param postContainerFrom {PostContainer}
     */
    addLink (postContainerFrom) {
      this.#links.push(postContainerFrom);
    }

    hasLinks () {
      return this.#links.length > 0;
    }

    isHome () {
      return this.#isHome;
    }

    /**
     * @param report {Report}
     */
    testValidPermalink (report) {
      const parts = this.#permalink.split('/');
      if (parts.length < 2) {
        return;
      }

      if (parts[parts.length - 2].match(/^\d+-\d+-\d+$/)) {
        report.addWarning('Invalid permalink given ' + this.#permalink + ' for «' + this.title + '», <a href="' + hostOrigin + '/wp-admin/post.php?post=' + this.postID + '&action=edit" target="_blank">Edit</a>');
      }
    }

    /**
     * @param report {Report}
     */
    testPreview (report) {
      let that = this;

      this.#$node.find('img[src]').each(function () {
        const $t = $(this);

        const src = $t.attr('src');
        if (src.match(/\/Vorschaubild.*\.jpg$/)) {
          report.addWarning('Preview image used in «' + that.title + '», <a href="' + hostOrigin + '/wp-admin/post.php?post=' + that.postID + '&action=edit" target="_blank">Edit</a>');
        } else if (src.match(/\/Beispiel-01.*\.jpg$/)) {
          report.addWarning('Sample image used in «' + that.title + '», <a href="' + hostOrigin + '/wp-admin/post.php?post=' + that.postID + '&action=edit" target="_blank">Edit</a>');
        } else if (src === '') {
          report.addWarning('Image without source in «' + that.title + '», <a href="' + hostOrigin + '/wp-admin/post.php?post=' + that.postID + '&action=edit" target="_blank">Edit</a>');
        }
      });
    }

    /**
     * @param report {Report}
     */
    testGallery (report) {
      const that = this;

      this.#$node.find('.wp-block-gallery').each(function () {
        const $images = $(this).find('figure.wp-block-image');
        const $lis = $(this).find('li');

        if ($lis.length === 0 && $images.length === 0) {
          report.addWarning('Empty gallery in «' + that.title + '», <a href="' + hostOrigin + '/wp-admin/post.php?post=' + that.postID + '&action=edit" target="_blank">Edit</a>');
        } else if ($(this).find('figure > a').length === 0) {
          report.addWarning('Gallery images have no links (Select link to media file) in «' + that.title + '», <a href="' + hostOrigin + '/wp-admin/post.php?post=' + that.postID + '&action=edit" target="_blank">Edit</a>');
        }
      });
    }
  }

  class PolyPostContainer {
    #postContainers = {};

    constructor (poly) {
      this.poly = poly;
    }

    /**
     * @param postContainer {PostContainer}
     */
    addPostContainer (postContainer) {
      this.#postContainers[postContainer.lang] = postContainer;
    }

    /**
     *
     * @param lang {string}
     * @returns {PostContainer|undefined}
     */
    getPostContainer (lang) {
      return this.#postContainers[lang];
    }

    /**
     *
     * @return {Object.<string>}
     */
    getPostContainers () {
      return this.#postContainers;
    }

    /**
     * @returns {boolean}
     */
    isHome () {
      for (const lang in this.#postContainers) {
        /** @var pc {PostContainer} */
        const pc = this.#postContainers[lang];
        if (pc.isHome()) {
          return true;
        }
      }

      return false;
    }

    /**
     * @param report {Report}
     */
    testMissingTranslation (report) {
      // We assume, that one of the two would exist for sure.
      if (this.#postContainers['de'] === undefined) {
        report.addWarning('Missing german translation for «' + this.#postContainers['fr'].title + '»');
      } else if (this.#postContainers['fr'] === undefined) {
        report.addWarning('Missing french translation for «' + this.#postContainers['de'].title + '»');
      }
    }

    /**
     * @param report {Report}
     */
    testMissingIncomingLinks (report) {
      if (this.isHome()) {
        return;
      }

      for (const lang in this.#postContainers) {
        /** @var pc {PostContainer} */
        const pc = this.#postContainers[lang];
        if (!pc.hasLinks()) {
          report.addWarning('No incoming links found for «' + pc.title + '»');
        }
      }
    }
  }

  const screenIntegrationTest = () => {
    const $body = $('body');
    const report = new Report();
    const languages = ['de', 'fr'];

    if ($body.data('init') !== true) {
      report.addWarning('The screen seems not to be initialized.');
      report.send();
      return;
    }

    let polyPostContainers = {};

    // Get all post containers.
    $('.post-container').each(function () {
      const $t = $(this);
      const poly = $t.data('poly');

      if (polyPostContainers[poly] === undefined) {
        polyPostContainers[poly] = new PolyPostContainer(poly);
      }
      polyPostContainers[poly].addPostContainer(new PostContainer($t));
    });

    let missingHome = true;
    for (const poly in polyPostContainers) {
      Object.entries(polyPostContainers[poly].getPostContainers())
        .forEach(([lang, postContainer]) => {
          /** @var postContainer {PostContainer} */
          if (postContainer.isHome()) {
            missingHome = false;
          }
        });
    }

    if (missingHome) {
      report.addWarning('HOME post is missing (Please add the tag `Home` to one of the posts in german language).');
    }

    ///
    // Check if every post does have a corresponding translation.
    ///
    for (const poly in polyPostContainers) {
      polyPostContainers[poly].testMissingTranslation(report);
    }

    ///
    // Check all hyperlinks (inside post containers).
    ///
    $('.post-container a').each(function () {
      const $t = $(this);

      const $postContainer = $t.closest('.post-container');
      const postContainer = polyPostContainers[$postContainer.data('poly')].getPostContainer($postContainer.data('lang'));
      const postInfo = '«' + postContainer.title + '», <a href="' + hostOrigin + '/wp-admin/post.php?post=' + postContainer.postID + '&action=edit" target="_blank">Edit</a>';

      const href = $t.attr('href');

      let linkText = $t.text();
      if (linkText === '') {
        const $img = $t.find('img');
        if ($img.length > 0) {
          linkText = 'img:' + $img.attr('src');
        } else {
          linkText = 'html:' + $t.html();
        }
      }

      if (href === undefined) {
        report.addWarning('Link without `href` [' + linkText + '], ' + postInfo);
        return;
      }

      if (href.indexOf('/wp-content/') >= 0) {
        // This link is considered valid, as it links most probable to an uploaded file.
        return;
      }

      if (href.indexOf('action=edit') >= 0) {
        // This link is a WordPress generated link to edit a post (would only be visible if logged in).
        return;
      }

      if ($t.data('ds-museum-map-toggle') !== undefined) {
        // This link is a toggle button used by the museum-map feature.
        return;
      }

      if ($t.closest('.leaflet-control').length > 0) {
        // This link is part of a leaflet (map) control, i.e. zoom-in, zoom-out.
        return;
      }

      if ($t.closest('.mejs-controls').length > 0) {
        // This link is part of the mediaelement-js player controls.
        return;
      }

      if ($t.data('action') === 'play-video') {
        // This link is a javascript generated link which would start the video player if clicked.
        return;
      }

      const $hrefContainer = $('.post-container[data-permalink="' + href + '"]');
      if ($hrefContainer.length === 0) {
        report.addWarning('Link [' + linkText + '](' + href + ') points to invalid post (maybe wrong permalink), ' + postInfo);
        return;
      }

      const hrefPoly = $hrefContainer.data('poly');
      const hrefLang = $hrefContainer.data('lang');

      // Check if the link points to a post of the same language.
      if (postContainer.lang !== hrefLang) {
        report.addWarning('Cross-Language Link: [' + linkText + '](' + href + ') points to a post of the other language, ' + postInfo);
      }

      polyPostContainers[hrefPoly].getPostContainer(hrefLang).addLink(postContainer);
    });

    if (!$body.is('.pm43f')) {
      // We do not check incoming links for the big main screens like `pm43f`, because they get a navigation anyway.
      for (const poly in polyPostContainers) {
        polyPostContainers[poly].testMissingIncomingLinks(report);
      }
    }

    for (const poly in polyPostContainers) {
      Object.entries(polyPostContainers[poly].getPostContainers())
        .forEach(([lang, postContainer]) => {
          postContainer.testValidPermalink(report);
        });
    }

    for (const poly in polyPostContainers) {
      Object.entries(polyPostContainers[poly].getPostContainers())
        .forEach(([lang, postContainer]) => {
          postContainer.testPreview(report);
        });
    }

    for (const poly in polyPostContainers) {
      Object.entries(polyPostContainers[poly].getPostContainers())
        .forEach(([lang, postContainer]) => {
          postContainer.testGallery(report);
        });
    }

    report.send();
  };

  $(function () {
    setTimeout(screenIntegrationTest, 250);
  });
})(window, document, jQuery);