(function ($) {
  $(function () {

    $('body').on('click', '[data-ds-action]', function (event) {
      event.preventDefault();

      switch ($(this).data('ds-action')) {
        case 'add':
          const html = $('#ds-museum-map-layers-template').html();
          $(this).closest('[data-ds-item]').after(
            '<div data-ds-item>' + html + '</div>'
          );
          break;
        case 'del':
          $(this).closest('[data-ds-item]').remove();
          break;
      }
    });

  });
})(jQuery);