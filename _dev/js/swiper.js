const ds_swiper = ((doc, $, _Swiper) => {
  'use strict';

  const _init = ($body) => {
    $body.on('ds-leave', function () {
      $('.swiper > .wp-block-group__inner-container').each(function () {
        const swiper = this['swiper'];
        if (swiper !== undefined) {
          swiper.slideTo(0, 0, false);
        }
      });
    });

    $body.find('.swiper > .wp-block-group__inner-container').each(function () {
      const $swiper = $(this);

      const $wrapper = $('<div class="swiper-wrapper"></div>');
      $swiper.append($wrapper);

      $swiper.find('.swiper-slide').appendTo($wrapper);
      $swiper.append('<div class="swiper-pagination"></div>');
      $swiper.append('<div class="swiper-button-prev">' + $('#icon-button-prev').html() + '</div><div class="swiper-button-next">' + $('#icon-button-next').html() + '</div>');

      // Construct the swiper components.
      new _Swiper('.swiper > .wp-block-group__inner-container', {
        autoHeight: true,
        loop: true,
        on: {
          slideChange: function (swiper) {
            const eventData = {
              'name': 'swiper.slideChange',
              'index': swiper.realIndex
            };

            doc.dispatchEvent(new CustomEvent('ds-action', { detail: eventData }));
          }
        },
        pagination: {
          el: '.swiper-pagination',
          type: 'fraction',
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        }
      });
    });
  };

  return { 'init': _init };
})(document, jQuery, Swiper);
