=== DS Theme ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 5.8
Tested up to: 6.1
Requires PHP: 7.4
Stable tag: 1.31
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The DS Theme.

== Description ==

The **DS Theme**.

== Changelog ==

= 1.32 =
* Add new world.

= 1.31 =
* Change museum map.

= 1.30 =
* Flip order of posts in category page.

= 1.29 =
* Remove PUCv4 updater support.

= 1.28 =
* Add PUCv5 updater support.

= 1.27 =
* Fix color for world 98.

= 1.26 =
* Refactor the museum-map.
* Add museum-map options to let users define the layers.
* Add a new world.

= 1.25 =
* Add touch animation for info panel screens.

= 1.24 =
* Fix some minor bugs.
* Fix screen integration tests.
* Set extra-margin-top and extra-margin-bottom as css variables.

= 1.23 =
* Add simple swiper support.

= 1.22 =
* Add styles for `strong` and `em` elements.

= 1.21 =
* Fix color for world-2.

= 1.20 =
* Fix museum plan.

= 1.19 =
* Add museum plan.

= 1.18 =
* Fix pointer events.

= 1.17 =
* Add flip book.

= 1.16 =
* Fix small audio bugs.

= 1.15 =
* Convert audio to use media element player.

= 1.14 =
* Add pattern for fav location.

= 1.13 =
* Enable audio block.
* Add fancybox for single images.

= 1.12 =
* Dispatch `ds-enter` event for home post container.

= 1.11 =
* Add a new helper css class for the world title row.

= 1.10 =
* Display posts in `draft` status for admin users.

= 1.9 =
* Fix overlay scrollbars for special "Neptun" screen.

= 1.8 =
* Add world 2.

= 1.7 =
* Add screen devices.

= 1.6 =
* Add and update styles and scripts.
* Add overlay scrollbars.

= 1.5 =
* Add `modified` to the api screens list.

= 1.4 =
* Add api to list all screens.

= 1.3 =
* Fix watchdog.

= 1.2 =
* Add post message.

= 1.1 =
* Use Puc Updater.

= 1.0 =
* Initial version.
