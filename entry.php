<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-title="<?php echo esc_attr(get_the_title()); ?>">

    <?php get_template_part('entry', 'content'); ?>

    <?php if (is_user_logged_in()) { ?>
        <div class="edit-post-link-container">
            <?php edit_post_link(); ?>
        </div>
    <?php } ?>

</article>
