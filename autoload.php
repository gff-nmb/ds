<?php

declare( strict_types=1 );

spl_autoload_register( function ( $class_name ): void {
	$parent_namespace = 'DS';
	$classes_folder   = 'includes';

	// Project namespace
	$project_namespace = $parent_namespace . '\\';
	$length            = strlen( $project_namespace );

	if ( 0 !== strpos( $class_name, $project_namespace ) ) {
		// Not our concern.
		return;
	}

	$classes_dir = DS_THEME_DIR_PATH . $classes_folder . DIRECTORY_SEPARATOR;

	// Remove top-level namespace (that is the current dir)
	$class_file = substr( $class_name, $length );
	// Swap underscores for dashes and lowercase
	$class_file = str_replace( '_', '-', strtolower( $class_file ) );

	// Prepend `class-` to the filename (last class part)
	$class_parts                = explode( '\\', $class_file );
	$last_index                 = count( $class_parts ) - 1;
	$class_parts[ $last_index ] = 'class-' . $class_parts[ $last_index ];

	// Join everything back together and add the file extension
	$class_file = implode( DIRECTORY_SEPARATOR, $class_parts ) . '.php';
	$location   = $classes_dir . $class_file;

	if ( ! is_file( $location ) ) {
		return;
	}

	include_once( $location );
} );
