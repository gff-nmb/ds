<?php

declare( strict_types=1 );

// Get the default language as well as a list of all languages defined.
$default_language = apply_filters( 'wpml_default_language', null );
$languages        = apply_filters( 'wpml_active_languages', null );

// Set the date format.
$date_format = 'Y-m-d H:i:s';

$screens = [];
foreach ( get_categories() as $category ) {
	$url = get_category_link( $category );
	$key = $category->name;

	$modified = null;
	foreach ( $languages as $language_code => $language ) {
		do_action( 'wpml_switch_language', $language_code );

		// Get the last modified post that is published and that is associated
		// with the current category (and for the current language).
		$query = new WP_Query( [
			'posts_per_page'   => 1,
			'offset'           => 0,
			'orderby'          => 'post_modified',
			'order'            => 'DESC',
			'category_name'    => $key,
			'post_type'        => 'post',
			'post_status'      => 'publish',
			'suppress_filters' => true
		] );

		while ( $query->have_posts() ) {
			$query->the_post();

			$post_modified = get_the_modified_date( $date_format );
			if ( $modified === null || $post_modified > $modified ) {
				$modified = $post_modified;
			}
		}

		wp_reset_postdata();
	}

	$screens[] = [
		'key'      => $key,
		'url'      => $url,
		'modified' => $modified
	];
}

wp_send_json( [
	'screens' => $screens,
] );
