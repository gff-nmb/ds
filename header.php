<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <script>
      window.addEventListener('error', function (error) {
        const eventData = {
          'name': 'error',
          'error': error
        };

        document.dispatchEvent(new CustomEvent('ds-action', { detail: eventData }));
      });
    </script>
	<?php wp_head(); ?>
    <meta name="msapplication-tap-highlight" content="no"/>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>