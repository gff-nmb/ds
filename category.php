<?php

declare( strict_types=1 );

use DS\Detector;
use DS\SVG_Utils;

if ( is_null( Detector::detect_device() ) ) {
	die( 'Sorry' );
}

get_header();

?>
    <div class="overlay" style="display:none"></div>

<?php
if ( get_theme_mod( 'ds_development_mode', false ) ) { ?>
    <div class="debug-device"></div>
    <div class="debug-contents"></div>
	<?php
} ?>

    <div class="page-wrapper">
        <div class="page-sidebar-container">
            <div class="page-sidebar-top">
                <div class="icon-button" style="display:none;"
                     data-action="go-back"><?php
					SVG_Utils::ds_svg( 'button-arrow-left' ) ?></div>
            </div>
            <div class="page-sidebar-bottom">
                <div class="icon-button" style="display:none;" data-action="switch-language"
                     data-lang="fr"><?php
					SVG_Utils::ds_svg( 'button-fr' ) ?></div>
                <div class="icon-button" style="display:none;" data-action="switch-language"
                     data-lang="de"><?php
					SVG_Utils::ds_svg( 'button-de' ) ?></div>
            </div>
        </div>
        <div class="page-post-container">

			<?php
			$default_language = apply_filters( 'wpml_default_language', null );
			$languages        = apply_filters( 'wpml_active_languages', null );

			$query_params = [
				'category_name' => get_query_var( 'category_name' ),
				'order'         => 'ASC',
			];

			$current_user = wp_get_current_user();
			if ( $current_user instanceof WP_User ) {
				if ( in_array( 'administrator', $current_user->roles ) ) {
					$query_params['post_status'] = [ 'publish', 'pending', 'draft', 'auto-draft', 'future' ];
				}
			}


			$param_lang = filter_input( INPUT_GET, 'lang' );

			$home_id = null;
			foreach ( $languages as $language_code => $language ) {
				do_action( 'wpml_switch_language', $language_code );
				$query = new WP_Query( $query_params );
				while ( $query->have_posts() ) {
					$query->the_post();

					// Get the ID of the post in the default language. This would be used in the frontend to detect translations of a specific post.
					$poly_id = apply_filters( 'wpml_object_id', $query->post->ID, 'post', false, $default_language );

					// Check if this should be the initial view (tagged as 'Home').
					$is_initial_view = false;
					if ( $query->post->ID === $home_id ) {
						$is_initial_view = true;
					} elseif ( $default_language === $language_code ) {
						$tags = get_the_tags();
						if ( is_array( $tags ) ) {
							foreach ( $tags as $tag ) {
								if ( $tag->name === 'Home' ) {
									if ( $param_lang !== null ) {
										$poly_param_lang_id = apply_filters(
											'wpml_object_id',
											$query->post->ID,
											'post',
											false,
											$param_lang
										);

										if ( $poly_param_lang_id !== null ) {
											$home_id = $poly_param_lang_id;
										} else {
											$is_initial_view = true;
										}
									} else {
										$is_initial_view = true;
									}
								}
							}
						}
					}


					echo '<div id="post-container-' . get_the_ID() . '" ';
					echo 'data-permalink="' . get_the_permalink() . '" ';
					echo( $is_initial_view ? 'data-home ' : '' );
					echo 'data-poly="' . $poly_id . '" data-lang="' . $language_code . '" class="post-container' . ( $is_initial_view ? ' active' : '' ) . '">';
					get_template_part( 'entry' );
					echo '</div>';
				}
				wp_reset_postdata();
			}
			?>
        </div>
    </div>

    <div id="icon-button-arrow-left" class="template">
        <div class="icon-button"><?php
			SVG_Utils::ds_svg( 'button-arrow-left' ); ?></div>
    </div>
    <div id="icon-button-arrow-right" class="template">
        <div class="icon-button"><?php
			SVG_Utils::ds_svg( 'button-arrow-right' ); ?></div>
    </div>
    <div id="icon-button-prev" class="template">
        <div class="icon-button"><?php
			SVG_Utils::ds_svg( 'button-prev' ); ?></div>
    </div>
    <div id="icon-button-next" class="template">
        <div class="icon-button"><?php
			SVG_Utils::ds_svg( 'button-next' ); ?></div>
    </div>
    <div id="flip-book-passepartout-must-see" class="template">
        <div class="flip-book-passepartout"><?php
			SVG_Utils::ds_svg( 'flip-book-must-see' );
			?></div>
    </div>
    <div id="flip-book-passepartout-real-biel" class="template">
        <div class="flip-book-passepartout"><?php
			SVG_Utils::ds_svg( 'flip-book-real-biel' );
			?></div>
    </div>
    <div id="flip-book-passepartout-secret-spot" class="template">
        <div class="flip-book-passepartout"><?php
			SVG_Utils::ds_svg( 'flip-book-secret-spot' );
			?></div>
    </div>

<?php
get_footer(); ?>