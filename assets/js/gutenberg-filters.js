"use strict";// noinspection DuplicatedCode
wp.domReady(()=>{wp.blocks.unregisterBlockStyle('core/image','rounded');wp.blocks.unregisterBlockStyle('core/image','default');wp.blocks.unregisterBlockStyle('core/button','outline');wp.blocks.registerBlockStyle('core/button',{name:'navigation',label:'Navigation'});// Remove the ability to add "inline images", "inline code" and "keyboard input" into
// for example the paragraphs.
wp.richText.unregisterFormatType('core/image');wp.richText.unregisterFormatType('core/code');wp.richText.unregisterFormatType('core/keyboard');});
